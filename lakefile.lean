prelude
import Lake
open Lake DSL

package LeanREPL {
}

-- @[defaultTarget]
lean_exe «ilean» {
  root := `Main
  supportInterpreter := true
}

